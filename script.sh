# Branch configs
TRUNK=$4
BRANCHES=$5
#EXTRA_BRANCHES="{_trunkOLD,ER506-Leasing,PacmanSF}"
EXTRA_BRANCHES=$6
echo "Migrating $SVN_PATH to $GIT_REPO"
echo "Creating directory $SVN_PATH"
mkdir -p $SVN_PATH
cd $SVN_PATH
echo "Initialising git repository"
git init
git svn init $SVN_SERVER/$SVN_PATH
echo "Setting SVN structure"
# Set-up svn branches
git config svn.authorsFile $AUTHORS
git config --replace-all svn-remote.svn.fetch "$TRUNK:refs/remotes/$SVN_REMOTE/master"
[ "$BRANCHES" ] && git config --add svn-remote.svn.branches "$BRANCHES/*:refs/remotes/$SVN_REMOTE/*"
[ "$EXTRA_BRANCHES" ] && git config --add svn-remote.svn.branches "$EXTRA_BRANCHES:refs/remotes/$SVN_REMOTE/*"
cat .git/config
echo "Beginning SVN fetch from $SVN_SERVER/$SVN_PATH"
git svn fetch
echo "SVN fetch done."
echo "Checkout local copies of all svn branches"
# Checkout local copies of all svn branches
git checkout master
remote=$SVN_REMOTE
for brname in $(
    git branch -r | grep $remote | grep -v master | grep -v HEAD | awk '{gsub(/^[^\/]+\//,"",$1); print $1}'
); do
    git branch -D $brname
    git checkout -b $brname $remote/$brname
done
git checkout master
#lowercase_reponame=$( echo "$GIT_REPO" | tr -s  '[:upper:]'  '[:lower:]' )
#echo $lowercase_reponame
echo "create remote repo"
curl -X POST -u ssjirasrvc:SmartAdm1n -H "Content-Type: application/json" https://api.bitbucket.org/2.0/repositories/smartsalary/$GIT_REPO -d '{"scm": "git", "is_private": true}'
echo "Pushing all local branches to $GIT_SERVER/$GIT_REPO"
# Push all local branches to git server
git remote add origin $GIT_SERVER/"$GIT_REPO".git
git push -u origin --all
echo "Script complete."
